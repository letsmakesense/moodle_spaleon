<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


use mod_spaleon\constants;

function spaleon_add_instance(stdClass $data) {
        global $DB;

    $data->timemodified = time();
    $data->courseid = $data->course;
    try {
        $data->id = $DB->insert_record(constants::DB_TABLE, $data);
    } catch (dml_exception $e) {
        $msg = $e->getMessage();
        print_error($msg);
    }
    return $data->id;

}
function spaleon_update_instance(stdClass $data) {
    global $DB;

    $data->timemodified = time();
    $data->id = $data->instance;
    return $DB->update_record(constants::DB_TABLE, $data);
}

function spaleon_delete_instance(int $id) {
    global $DB;

    $activity = $DB->get_record(constants::DB_TABLE, ['id' => $id]);
    if (!$activity) {
        return false;
    }

    $DB->delete_records(constants::DB_TABLE, ['id' => $id]);

    return true;
}
