<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 *
 *
 * @author Let's make sense GmbH | Peter Klein
 * @since 24.03.2021
 */

namespace mod_spaleon;


class constants
{
    const MODULE = 'mod_spaleon';
    const MOD_DB_NAME = 'spaleon';
    const DB_TABLE = 'spaleon';

    const ENDPOINT = 'endpoint';
    const DEFAULT_ENDPOINT = 'https://www.spaleon.org';
    const CLIENT_ID = 'client_id';
    const CLIENT_SECRET = 'client_secret';
}
