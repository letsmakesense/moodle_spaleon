<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 *
 *
 * @author Let's make sense GmbH | Peter Klein
 * @since 24.03.2021
 */

namespace mod_spaleon;


use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;

class external extends \external_api
{
    public static function get_history_parameters()
    {
        return new external_function_parameters([
            'user_id' => new external_value(PARAM_INT, 'ID of the current user')
        ]);
    }

    public static function get_history()
    {
        return [
            [
                'id' => 1,
                'insert_date' => 1616623195,
                'data' => '{"verb": "caminar", "tempus": "presente"}'
            ]
        ];
    }

    public static function get_history_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'record id'),
                    'insert_date' => new external_value(PARAM_INT, 'timestamp of the activity'),
                    'data' => new external_value(PARAM_TEXT, 'values json encoded'),
                )
            )
        );
    }

}
