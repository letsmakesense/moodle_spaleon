<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
use mod_spaleon\constants;

require('../../config.php');
require_once('lib.php');


/** @var moodle_database $DB */
/** @var moodle_page $PAGE */
/** @var core_renderer $OUTPUT */
/** @var stdClass $USER */

try {
    $id = required_param('id', PARAM_INT);
    list ($course, $cm) = get_course_and_cm_from_cmid($id, constants::MOD_DB_NAME);
    $course_specs = $DB->get_record(constants::DB_TABLE, array('id'=> $cm->instance), '*', MUST_EXIST);
    // set url including parameters to redirect correctly after language switch
    $PAGE->set_url(new moodle_url('/mod/'.constants::MOD_DB_NAME.'/view.php?id='.$id));
    // checks if user is logged in and redirects to login page if not
    require_course_login($course, true, $cm);
    // initializes context with course module id
    // this adds breadcrumb navigation to the header
    $context = context_module::instance($cm->id);
    $PAGE->set_context($context);
    $title = ucfirst(strtolower(get_string($course_specs->verb_tense, constants::MODULE)));
    $PAGE->set_title(get_string('pluginname', constants::MODULE));
    $PAGE->set_heading($title);
    $PAGE->set_pagelayout('course');
    echo $OUTPUT->header();


    $context = [
        // get_config reads values defined in plugin settings
        "client_id" => get_config(constants::MODULE, constants::CLIENT_ID),
        "title" => $title,
        "date" => userdate(time()),
        "activity_url" => new moodle_url($PAGE->url, ['id' => $id])
    ];
    if ($USER->id > 0) {
        $context["user_name"] = $USER->username;
        $context["user_id"] = $USER->id;
    }
    echo $OUTPUT->render_from_template(constants::MODULE . '/entry', $context);
    echo $OUTPUT->footer();
} catch (coding_exception $e) {
    echo sprintf('found exception %s', $e);
} catch (moodle_exception $e) {
    if ($e->getCode() == 'missingparam') {
        try {
            redirect(new moodle_url('/my/'));
        } catch (Exception $e) {
            echo sprintf('found moodle exception %s', $e);
        }
    } else {
        echo sprintf('found moodle exception %s', $e);
    }

}

