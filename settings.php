<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Adds admin settings for the plugin.
 *
 * @package     mod_spaleon
 * @category    admin
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use mod_spaleon\constants;

if ($ADMIN->fulltree) {
    $settings->add(
        new admin_setting_configtext_with_maxlength(
            constants::MODULE . '/' . constants::ENDPOINT,
            get_string(constants::ENDPOINT, constants::MODULE),
            get_string(constants::ENDPOINT . '_desc', constants::MODULE),
            constants::DEFAULT_ENDPOINT,
            PARAM_TEXT,
            100
        )
    );
    $settings->add(
        new admin_setting_configtext_with_maxlength(
            constants::MODULE.'/'.constants::CLIENT_ID,
            get_string(constants::CLIENT_ID, constants::MODULE),
            get_string(constants::CLIENT_ID.'_desc', constants::MODULE),
            '',
            PARAM_TEXT,
            100
        )
    );
    $settings->add(
        new admin_setting_configtext(
            constants::MODULE.'/'.constants::CLIENT_SECRET,
            get_string(constants::CLIENT_SECRET, constants::MODULE),
            get_string(constants::CLIENT_SECRET.'_desc', constants::MODULE),
            '',
            PARAM_TEXT,
            100
        )
    );
}
