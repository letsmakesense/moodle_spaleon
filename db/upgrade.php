<?php

use mod_spaleon\constants;

/**
 * Actions to execute when database tables or fields where changed
 *
 * For every new version a new upgrade function needs to be added
 *
 * @param int $oldversion
 * @throws ddl_exception
 * @throws ddl_table_missing_exception
 * @throws downgrade_exception
 * @throws upgrade_exception
 */
function xmldb_spaleon_upgrade($oldversion=0) {
    global $CFG, $DB;

    require_once($CFG->libdir.'/db/upgradelib.php');

    $dbman = $DB->get_manager();

    // upgrade having database migrations
    if ($oldversion < 2021031000) {
        upgrade_to_2021031000($dbman);
    }
    // update without database changes (e.g. global settings modified)
    if ($oldversion < 2021032500) {
        upgrade_mod_savepoint(true, '2021032500', constants::MOD_DB_NAME);
    }
}

function upgrade_to_2021031000($dbman) {
    $table = new xmldb_table(constants::DB_TABLE);
    $field = new xmldb_field(('verb_tense'));
    $field->set_attributes(XMLDB_TYPE_CHAR, '32', null, XMLDB_NOTNULL, null, null);
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    upgrade_mod_savepoint(true, '2021031000', constants::MOD_DB_NAME);
}
