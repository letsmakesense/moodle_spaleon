<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    $string['pluginname'] = 'Spaleon';
    $string['pluginadministration'] = 'Spaleon - Admin';
    $string['modulename'] = 'Spaleon';
    $string['modulenameplural'] = 'Spaleon';

    $string['manage'] = 'Spaleon API Zugang';
    $string['endpoint'] = 'Service Endpoint';
    $string['endpoint_desc'] = 'Endpoint der Web-API von spaleon.org';
    $string['client_id'] = 'ClientID';
    $string['client_id_desc'] = 'Die ClientID deines Accounts bei spaleon.org';
    $string['client_secret'] = 'ClientSecret';
    $string['client_secret_desc'] = 'Das ClientSecret deines Accounts spaleon.org';



    $string['presente'] = 'Presente';
    $string['preterito'] = 'Preterito';
    $string['verb_tense'] = 'Zeitform';
    $string['select_tense'] = 'Wähle eine Zeitform';

    $string['view_headline'] = '{$a->title}-Übungen für {$a->user_name} ({$a->user_id}) auf "{$a->client_id}"';
