<?php
// This file is part of 'Spaleon Plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    $string['pluginname'] = 'Spaleon';
    $string['pluginadministration'] = 'Spaleon - Admin';
    $string['modulename'] = 'Spaleon';
    $string['modulenameplural'] = 'Spaleon';

    $string['manage'] = 'Spaleon API access';
    $string['endpoint'] = 'Service Endpoint';
    $string['endpoint_desc'] = 'Web service base url of spaleon.org';
    $string['client_id'] = 'ClientID';
    $string['client_id_desc'] = 'The client id provided by your account at spaleon.org';
    $string['client_secret'] = 'ClientSecret';
    $string['client_secret_desc'] = 'The client secret provided by your account at spaleon.org';



    $string['presente'] = 'Present Tense';
    $string['preterito'] = 'Preterite';
    $string['verb_tense'] = 'Verb Tense';
    $string['select_tense'] = 'Select verb tense';

     $string['view_headline'] = '{$a->title} exercises for {$a->user_name} ({$a->user_id}) at "{$a->client_id}"';
