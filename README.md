# Basic Moodle activity module using web components

Read this [tutorial](https://docs.moodle.org/dev/Activity_modules) to understand how Moodle activity modules work.

This repository creates an example activity module which can be added to Moodle courses. It integrates a web component into a standard Moodle template.

## Features ##

Currently, the following features are implemented:

 * Basic activity module requirements:
    * Database structure definitions and functions to execute database migrations on updates
    * Language files for string translation
    * Mustache templates which could be used with PHP and Javascript
    * A library to add module instances to the Moodle course database
    * A form to modify module settings when adding the plugin to a course
    * A view which selects the course instance of the plugin and displays personalized data
    * An icon to identify the plugin in activity selections
 * Custom web component: 
    * Simple integration by just adding a tag and the script source link
    * Communication with an external backend and a local Moodle web service
    * Fast interaction as no full page reloads are required
    * Web component might be used in other Learning Management Systems as well
 * Global plugin settings:
    * defines an endpoint for the external backend
    * adds API credentials for the external service
 * Moodle web service:
    * provides filtered database access to Javascript modules
    
## Use as template ##

If you want to use the current state of the plugin as a starting point for your activity plugin you need to make changes in these places:

 * replace namespace 'mod_spaleon'
 * modify constants in 'classes/constants.php'
 * adjust database definitions in db/install.xml
 * change course instance creation in lib.php
 * create your own language files in the 'lang' folder
 * change view.php and templates/entry.mustache according to your needs
 * edit course settings in mod_form.php and plugin settings in settings.php






