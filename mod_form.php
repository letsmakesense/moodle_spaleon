<?php
// This file is part of 'Spaleon plugin'
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use mod_spaleon\constants;

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

/** @var stdClass $CFG */
require_once($CFG->dirroot.'/course/moodleform_mod.php');

class mod_spaleon_mod_form extends moodleform_mod {

    function definition() {
        global $CFG, $DB, $OUTPUT;

        $mform =& $this->_form;

        $options = array('presente' => get_string('presente', constants::MODULE),
                           'preterito' => get_string('preterito', constants::MODULE));
        $mform->addElement('select', 'verb_tense', get_string('select_tense', constants::MODULE), $options);
        $mform->setDefault('verb_tense', 'presente');

        $this->standard_coursemodule_elements();

        $this->add_action_buttons();
    }
}
